package com.stgconsulting.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloCounterApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloCounterApplication.class, args);
	}
}
