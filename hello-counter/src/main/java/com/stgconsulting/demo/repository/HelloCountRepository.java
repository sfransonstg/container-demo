package com.stgconsulting.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.stgconsulting.demo.domain.HelloCount;

public interface HelloCountRepository extends CrudRepository<HelloCount, String> {

}
